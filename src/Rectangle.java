public class Rectangle {
    float length = 1.0f;
    float width = 1.0f;
    // hàm được gọi khi không truyền tham số
    public Rectangle(){
        super();
    }
    //hàm được gọi khi truyền tham số
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
     // hàm được gọi khi tính diện tích
     public float getArea() {
        return (length * width);
    }

    // hàm được gọi khi tính chu vi
    public float getCircumference() {
        return (length + width)*2;
    }

    // getter setter
    public float getLength() {
        return length;
    }
    public void setLength(float length) {
        this.length = length;
    }
    public float getWidth() {
        return width;
    }
    public void setWidth(float width) {
        this.width = width;
    }
    // to string
    @Override
    public String toString() {
        return "Rectangle [length=" + length + ", width=" + width + "]";
    }
    

}
