public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Trần văn ninh");
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3,5);
        // chu vi diện tích hình chữ nhật khi không có tham số
        System.out.println("diện tích hình chữ nhật là "+rectangle1.getArea());
        System.out.println("chu vi hình chữ nhật là "+rectangle1.getCircumference());
        //chu vi diện tích hình chữ nhật khi có tham số
        System.out.println("diện tích hình chữ nhật là "+rectangle2.getArea());
        System.out.println("chu vi hình chữ nhật là "+rectangle2.getCircumference());
        // chiều dài chiều rộng hình chữ nhật
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
    }
}
